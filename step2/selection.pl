#! /usr/bin/perl
my $deleted;
$removed = 0;
#$char = chr(89);$num = ord($char); print "$num -- $char \n";exit;
$query = $ARGV[0];
#$output_file = $query.".s";unlink ("$output_file");
$output_file = $ARGV[1];
$substitution_threshold = $ARGV[2];

#list of amino acids
%acids=('65'=>0,'82'=>1,'78'=>2,'68'=>3,'67'=>4,'81'=>5,'69'=>6,'71'=>7,'72'=>8,'73'=>9,'76'=>10,'75'=>11,'77'=>12,'70'=>13,'80'=>14,'83'=>15,'84'=>16,'87'=>17,'89'=>18,'86'=>19);
%blosum62_mutation=('65'=>64.985,'82'=>163.917,'78'=>418.391 ,'68'=>417.982,'67'=>8107.115,'81'=>168.400,'69'=>171.771,'71'=>408.163,'72'=>2996.547,'73'=>87.866,'76'=>75.477,'75'=>166.186,'77'=>166.822,'70'=>430.877,'80'=>1100.255,'83'=>70.317,'84'=>158.486,'87'=>59885.831,'89'=>1134.567,'86'=>84.199);
# %acids=('A'=>0,'R'=>1,'N'=>2,'D'=>3,'C'=>4,'Q'=>5,'E'=>6,'G'=>7,'H'=>8,'I'=>9,'L'=>10,'K'=>11,'M'=>12,'F'=>13,'P'=>14,'S'=>15,'T'=>16,'W'=>17,'Y'=>18,'V'=>19);
# %blosum62_mutation=('A'=>64.985,'R'=>163.917,'N'=>418.391 ,'D'=>417.982,'C'=>8107.115,'Q'=>168.400,'E'=>171.771,'G'=>408.163,'H'=>2996.547,'I'=>87.866,'L'=>75.477,'K'=>166.186,'M'=>166.822,'F'=>430.877,'P'=>1100.255,'S'=>70.317,'T'=>158.486,'W'=>59885.831,'Y'=>1134.567,'V'=>84.199);
@blosum62=(["54.598","0.368","0.135","0.135","1.000","0.368","0.368","1.000","0.135","0.368","0.368","0.368","0.368","0.135","0.368","2.718","1.000","0.050","0.135","1.000"],["0.368","148.413","1.000","0.135","0.050","2.718","1.000","0.135","1.000","0.050","0.135","7.389","0.368","0.050","0.135","0.368","0.368","0.050","0.135","0.050"],["0.135","1.000","403.429","2.718","0.050","1.000","1.000","1.000","2.718","0.050","0.050","1.000","0.135","0.050","0.135","2.718","1.000","0.018","0.135","0.050"],["0.135","0.135","2.718","403.429","0.050","1.000","7.389","0.368","0.368","0.050","0.018","0.368","0.050","0.050","0.368","1.000","0.368","0.018","0.050","0.050"],["1.000","0.050","0.050","0.050","8103.084","0.050","0.018","0.050","0.050","0.368","0.368","0.050","0.368","0.135","0.050","0.368","0.368","0.135","0.135","0.368"],["0.368","2.718","1.000","1.000","0.050","148.413","7.389","0.135","1.000","0.050","0.135","2.718","1.000","0.050","0.368","1.000","0.368","0.135","0.368","0.135"],["0.368","1.000","1.000","7.389","0.018","7.389","148.413","0.135","1.000","0.050","0.050","2.718","0.135","0.050","0.368","1.000","0.368","0.050","0.135","0.135"],["1.000","0.135","1.000","0.368","0.050","0.135","0.135","403.429","0.135","0.018","0.018","0.135","0.050","0.050","0.135","1.000","0.135","0.135","0.050","0.050"],["0.135","1.000","2.718","0.368","0.050","1.000","1.000","0.135","2980.958","0.050","0.050","0.368","0.135","0.368","0.135","0.368","0.135","0.135","7.389","0.050"],["0.368","0.050","0.050","0.050","0.368","0.050","0.050","0.018","0.050","54.598","7.389","0.050","2.718","1.000","0.050","0.135","0.368","0.050","0.368","20.086"],["0.368","0.135","0.050","0.018","0.368","0.135","0.050","0.018","0.050","7.389","54.598","0.135","7.389","1.000","0.050","0.135","0.368","0.135","0.368","2.718"],["0.368","7.389","1.000","0.368","0.050","2.718","2.718","0.135","0.368","0.050","0.135","148.413","0.368","0.050","0.368","1.000","0.368","0.050","0.135","0.135"],["0.368","0.368","0.135","0.050","0.368","1.000","0.135","0.050","0.135","2.718","7.389","0.368","148.413","1.000","0.135","0.368","0.368","0.368","0.368","2.718"],["0.135","0.050","0.050","0.050","0.135","0.050","0.050","0.050","0.368","1.000","1.000","0.050","1.000","403.429","0.018","0.135","0.135","2.718","20.086","0.368"],["0.368","0.135","0.135","0.368","0.050","0.368","0.368","0.135","0.135","0.050","0.050","0.368","0.135","0.018","1096.633","0.368","0.368","0.018","0.050","0.135"],["2.718","0.368","2.718","1.000","0.368","1.000","1.000","1.000","0.368","0.135","0.135","1.000","0.368","0.135","0.368","54.598","2.718","0.050","0.135","0.135"],["1.000","0.368","1.000","0.368","0.368","0.368","0.368","0.135","0.135","0.368","0.368","0.368","0.368","0.135","0.368","2.718","148.413","0.135","0.135","1.000"],["0.050","0.050","0.018","0.018","0.135","0.135","0.050","0.135","0.135","0.050","0.135","0.050","0.368","2.718","0.018","0.050","0.135","59874.142","7.389","0.050"],["0.135","0.135","0.135","0.050","0.135","0.368","0.135","0.050","7.389","0.368","0.368","0.135","0.368","20.086","0.050","0.135","0.135","7.389","1096.633","0.368"],["1.000","0.050","0.050","0.050","0.368","0.135","0.135","0.050","0.050","20.086","2.718","0.135","2.718","0.368","0.135","0.135","1.000","0.050","0.368","54.598"]);

############################################################### BEGIN : main program
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time);
$temps = "##################### \n starting: ".$mday."  ".$hour.":".$min.":".$sec." \n";
print $temps."\n";

print "########parsing...\n";
&get_ids_nodes_edges_nbr($query);

if(scalar(@t_ids) > 1){	
	print "########sorting subgraphs by self substitution scores...\n";
	@sorted_list = &sort_by_mutation_power(\@t_ids);
	print "########substitution check...\n";
	&graph_substitution(@sorted_list);
}

print "######writing results in the output file...\n";
&save_final_subgraph_list($output_file, \@t_ids);


($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time);
$temps .= "end writing: ".$mday."  ".$hour.":".$min.":".$sec." \n ##################### \n";
print $temps;
$nb_subg = scalar(@t_ids);
print "query: $query -- $nb_subg subgraphs were found, $removed removed , still : ".($nb_subg-$removed)."\n";
print $deleted{"t # 110787 * 76"};
############################################################### END : main program

###############################################################  BEGIN : get_ids_nodes_edges_nbr
##sorting frequent subgraphs by size
sub get_ids_nodes_edges_nbr{
#create a table following this schema  $t_ids[num][option](option = 0:subgraph_id, 1:nodes_number, 2:edges_number)
#create a table for each subgraph containing its edges list: @{$edges_list_{$name}} where $name stands for the name of the subgraph
	$current_subgraph = 0;
	open(TOTO,$_[0]);
	while(<TOTO>){
	
		$c1=$_;$c1 =~ s/\s+$//;
		chomp($c1);
		if($c1 =~ /^t/) {
			$name = $c1;
			$t_ids[$current_subgraph] = $c1;
			$current_subgraph++;
		}
		else{
			if($c1 =~ /^v/) {
					@t = split(" ",$c1);
					push(@{$nodes_list_{$name}} ,@t[2]);
			}
			elsif($c1 =~ /^e/) {
					$edges_list_{$name} .= "$c1\n";
			}
			elsif($c1 =~ /^x/) {
					$fgs_list_{$name} = $c1;
			}
		}
	}
	close(TOTO);
}
###############################################################  END : get_ids_nodes_edges_nbr

###############################################################  BEGIN : get_nodes_number
sub get_nodes_nbr{
	$id = $_[0];
	return scalar(@{$nodes_list_{$id}});
}
###############################################################  END : get_nodes_number
###############################################################  BEGIN : get_edges_number
# sub get_edges_nbr{
	# $id = $_[0];
	# return scalar(@{$edges_list_{$id}});
# }
###############################################################  END : get_edges_number

###############################################################  BEGIN : sort_by_mutation_power
sub sort_by_mutation_power{

	my @tab2 = @{$_[0]};

	$t = scalar(@tab2);
	for($i=0;$i<$t;$i++){	
		push(@mutation ,[$tab2[$i], &mutation_score($tab2[$i])]);	
	}
	# rank (desc) subgraphs according to their mutation power
	@mutation = reverse sort { $a->[1] <=> $b->[1] } @mutation;
		return @mutation;
}
###############################################################  END : sort_by_mutation_power

###############################################################  BEGIN : mutation_score
sub mutation_score{
        $subg = $_[0];
        $s = scalar(@{$nodes_list_{$subg}});
        $mutation_prod = 1;

        for($i3=0;$i3<$s;$i3++){
                #get the label of nodes
                $t2 = $nodes_list_{$subg}[$i3];
				# $sub_n = chr(@t2[2]);$sub_n = uc($sub_n);
				$sub_n = $t2;#$sub_n = uc($sub_n);
				$mutation_prod *= (&two_nodes_substitution($t2,$t2) / $blosum62_mutation {"$sub_n"});
        }
        return (1 - $mutation_prod);
}
###############################################################  END : mutation_score

###############################################################  BEGIN : graph_substitution
sub graph_substitution{
	my (@tab) = @_;
	
	for($i=0;$i<scalar(@tab)-1;$i++){
		#if mutation_score ==0 then this motif can't mutate to any other; skipp;
		$n0 = $tab[$i][0];
		# $mutation_score = $tab[$i][2];
		# if( $mutation_score <=0 ){ next; }
		if($deleted{"$n0"}==1){ next; }
		@lst0 = split(" ",$fgs_list_{$n0});
		
		#check if this subgraph have been already substituted and removed, if so then skip it
		for($j=$i+1;$j<scalar(@tab);$j++){

			$n1 = $tab[$j][0];
			# $mutation_score2 = $tab[$j][2];
			# if( $mutation_score <=0 ){ next; }
			if($deleted{"$n1"}==1){ next;}

			#passing the names of the must be tested subgraphs
			$substitution_score = &two_graphs_substitution($n0,$n1);

			print "score = $substitution_score \n";
			if($substitution_score <0) {next;}
			if($substitution_score >= $substitution_threshold){
					@lst1 = split(" ",$fgs_list_{$n1});

					#merge the 2 appearence lists
					for($k=0;$k<scalar(@lst1);$k++){
						if(!(grep $_ eq $lst1[$k], @lst0)){
							push( @lst0, $lst1[$k]);
						}
					}
					#mark as substituted
					$deleted{"$n1"}=1;
						# print $deleted{"$n1"}." -- $n0 subst $n1 \n";
					#optimization function just to free substituted subgraphs					
					# &delete_subgraph($n1);
					# $j--;
			}
		}
		# sort(@lst0);
		$fgs_list_{$n0} = join(" ",@lst0);
	}
	return;
#print " number of substitutions : $nb_subst \n"; exit;
# return @accepted_subgraphs;
}
###############################################################  END : graph_substitution

###############################################################  BEGIN : two_graphs_substitution
#return -1 if the subgraphs are not substitutable 
#return $score otherwise
sub two_graphs_substitution{
	$sg1 = $_[0];
	$sg2 = $_[1];

	#first we verify if both subgraph have exactly the same shape
	# $shape = &verif_shape($sg1,$sg2); 
	# if($shape == -1){	return -1;	}
	if($edges_list_{$sg1} ne $edges_list_{$sg2}){	return -1;	}

	#le test de la substitution
	$subg_score = 0;

	$s = scalar(@{$nodes_list_{$sg1}});
	for($i3=0;$i3<$s;$i3++){
		#pour le moment on suppose que les nodes sont par defaut ordonnes dans le tableau @{$nodes_list_{$name}}
		#get the label of both nodes
		$t1 = $nodes_list_{$sg1}[$i3];
		$t2 = $nodes_list_{$sg2}[$i3];
		# $subg_score += &two_nodes_substitution($t1,$t2);
		$subg_score += (&two_nodes_substitution($t1,$t2) / &two_nodes_substitution($t1,$t1));
	}
	return (($subg_score/$s)*100);

}
###############################################################  END : two_graphs_substitution

###############################################################  BEGIN : two_nodes_substitution
sub two_nodes_substitution{
	#this line should be activated when labels are in ascii and not characters
	$node1 = $_[0];$node2 = $_[1];
	# $node1 = chr($_[0]);$node2 = chr($_[1]);
	# $node1 = uc($node1);
	# $node2 = uc($node2);
		$i1 = $acids{"$node1"};
		$i2 = $acids{"$node2"};
# print "$i1 -- $i2 -- $node1 -- $node2 \n";
	if(($i1 eq "")||($i2 eq "")){print "error : undefined label\n"; exit;}
	return $blosum62[$i1][$i2];
}
###############################################################  END : two_nodes_substitution

###############################################################  BEGIN : verif_shape
# this function is to verify if both subgraph have exactly the same shape or not based on edges
sub verif_shape{
	$sg1 = $_[0];
	$sg2 = $_[1];
	$s1 = $edges_list_{$sg1};
	$s2 = $edges_list_{$sg2};
	#if not same number of edges then not need to verify the shape; return -1; 
	if($s1 ne $s2){return -1;}
	#if there exist a difference; return -1; 
	# for($i4=0;$i4<$s1;$i4++){
		# if ($edges_list_{$sg1}[$i4] ne $edges_list_{$sg2}[$i4]){ return -1;}
	# }
	#everything is good; same shape; return 0; 
	return 0;
}
###############################################################  END : verif_shape

sub save_final_subgraph_list{

	my $output_f = $_[0];
	my @subg_list = @{$_[1]};;
	$out_str = "";
	
	for($l=0;$l<scalar(@subg_list);$l++){
		$name = $subg_list[$l];
		if ($deleted{"$name"}==1){
			$removed++;
			next;
		}
		$out_str .= $name ."\n";
		for($ll=0;$ll<scalar(@{$nodes_list_{$name}});$ll++){
			$n = $nodes_list_{$name}[$ll];
			$out_str .= "v $ll $n\n";
		}
		$out_str .= $edges_list_{$name};
		$out_str .= $fgs_list_{$name}."\n";
	}

	open(DAT, ">$output_f");
	print DAT $out_str; 
	close(DAT);
}
###############################################################  END : save_final_subgraph_list
